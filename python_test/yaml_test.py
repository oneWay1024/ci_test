import yaml

f = open('test.yaml', encoding="utf-8")
data = yaml.full_load(f)
print(data)

"""
{'however': 'A string, enclosed in quotes.', 
'Keys can be quoted too.': "Useful if you want to put a ':' in your key.", 
'single quotes': "have 'one' escape pattern",
 'double quotes': 'have many: ", \x00, \t, ☺, \r\n == \r\n, and more.'}

"""